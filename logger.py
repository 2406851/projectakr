import logging
from logging.handlers import TimedRotatingFileHandler
import datetime


def setup_logger():
    # Set up the logger
    logger = logging.getLogger("logger")
    logger.setLevel(logging.INFO)

    # Define the log format
    formatter = logging.Formatter("%(asctime)s - %(levelname)s : %(message)s")

    # Create a TimedRotatingFileHandler with daily rotation
    log_filename = "Logs/" + datetime.datetime.now().strftime("%Y-%m-%d") + ".log"
    # Thie file name is set to Logs/Y-M-D.log
    file_handler = TimedRotatingFileHandler(log_filename, when="midnight", interval=1, backupCount=7)
    # interval=1 -> new log file every day
    # backupCount=7 -> last 7 log files will be kept
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(formatter)

    # Add the handler to the logger
    logger.addHandler(file_handler)

    return logger
