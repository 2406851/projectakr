import logging


def save_prime_to_file(prime):
    file_name = input("Enter a file name: ")
    file_name += ".txt"  # Creates a .txt file name
    f = open(file_name, "w")
    f.write(str(prime))
    f.close()  # Writes the number and closes the file
    print("Your number %d was saved to a file named %s" % (prime, file_name))
    logger = logging.getLogger("logger")
    logger.info("Prime was saved to file %s" % file_name)


def read_prime_from_file():
    file_name = input("Enter a file name: ")
    file_name += ".txt"
    f = None
    prime = None
    logger = logging.getLogger("logger")
    try:  # Tries to open the file, entered by user
        f = open(file_name, "r")
        prime = int(f.read())  # Reads the number
    except FileNotFoundError:  # Catches FileNotFound error
        print("File with name '%s' does not exist\nTry a different name" % file_name)
        logger.warning("Failed to load number from file %s" % file_name)
    else:
        f.close()  # Closes the file
    if prime is not None:
        logger.info("Number was loaded from file %s" % file_name)
    return prime
